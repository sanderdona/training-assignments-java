package nl.cavero.warmup;

/**
 * Write a small program that counts from 1 to 100.
 *
 * For every multiple of 5, print the letter 'C'
 * For every multiple of 6, print the letter 'a'
 * For every multiple of 7, print the letter 'v'
 * For every multiple of 8, print the letter 'e'
 * For every multiple of 9, print the letter 'r'
 *
 * If a certain number is a divisible of at least two of the above numbers,
 * so for example 30 can be divided by both 5 and 6, then print 'Cavero is Awesome',
 * but also 18 can be divided by both 6 and 9, so there also print 'Cavero is Awesome'.
 *
 * If a number if not divisible by any of the above numbers,
 * then simply print the number itself (so you'd get 1, 2, 3, 4, C, a, etc)
 *
 * Please note that it's not necessary for this code to be fast or performant,
 * so don't worry about optimizing. The code is expected to be nice and neat
 * however: make it as lean, maintainable and clean as you can.
 *
 * Hint: should you find it difficult to come up with code that takes care of
 * all variations at once, just start out with printing the numbers from 1 to 100
 * and replacing the 5 and the 6 by the letters 'C' and 'a' respectively and
 * work your way forward from there.
 *
 */
public class CaveroInterview {

    public static void main(String[] args) {

        StringPrinter.print("Caver","Cavero is awesome", 5);

    }

}
