package nl.cavero.warmup;

/**
 * Created by Sander Dona on 6-6-2016.
 */
public class StringPrinter {

    public static void print(String word, String sentence, int startAt) {
        int timesPrinted;
        String printString = null;

        for (int i = 1; i <= 100; i++) {

            timesPrinted = 0;

            for (int x = startAt; x < startAt + word.length(); x++) {
                if(i % x == 0) {
                    printString = String.valueOf(word.charAt(x - startAt));
                    timesPrinted++;
                }
            }

            if(timesPrinted == 1) {
                System.out.println(printString);
            }else if(timesPrinted > 1) {
                System.out.println(sentence);
            }else {
                System.out.println(i);
            }

        }
    }
}
