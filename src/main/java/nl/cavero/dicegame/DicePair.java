package nl.cavero.dicegame;

/**
 * Created by Sander Dona on 7-6-2016.
 */
public class DicePair {
    private Dice dice1 = new Dice(6);
    private Dice dice2 = new Dice(6);
    private int lastRoll;

    public int getLastRoll() {
        return lastRoll;
    }

    public void rollDicePair() {
        lastRoll = dice1.rollDice() + dice2.rollDice();
    }
}
