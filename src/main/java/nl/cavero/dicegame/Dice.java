package nl.cavero.dicegame;

/**
 * Created by Sander Dona on 3-6-2016.
 */
public class Dice {
    private final int diceFaces;

    public Dice(int diceFaces) {
        this.diceFaces = diceFaces;
    }

    public int rollDice() {
        return (int) (Math.random() * diceFaces) + 1;
    }
}
