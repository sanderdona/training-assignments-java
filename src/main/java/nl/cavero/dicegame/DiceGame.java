package nl.cavero.dicegame;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * A simple dice game.
 * 
 * <p>
 * The objective of this game is to find the number of dice rolls necessary to get to the desired number.
 * The game is played with two dice. The number found should be printed on the screen.
 * 
 * <p>
 * You are free to create as many methods and classes as you like to write up the code. It's not necessary
 * to restrict yourself to only this . Additionally, you can use any library you find appropriate,
 * except of course one that would have this exact functionality...
 * 
 * <p>
 * Remark: As this code is driven by (semi-)random behavior, i.e. the roll of dice, we intend not to discuss
 * the outcome, but the structure of the code and the chosen solution method. We therefore haven't provided
 * any test cases to demonstrate the correct behavior.
 *
 */
public class DiceGame {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int desiredNumber = 0, totalRolls = 0;
        DicePair dicepair = new DicePair();

        System.out.println("Welcome!");

        boolean validInput = false;
        while (!validInput) {
            System.out.println("Enter the desired number:");
            try {
                desiredNumber = scanner.nextInt();
                validInput = true;
            } catch (InputMismatchException e) {
                validInput = false;
                System.out.println("Wrong input!");
                scanner.nextLine();
            }
        }

        while ((desiredNumber != dicepair.getLastRoll())){
            dicepair.rollDicePair();
            totalRolls++;
            System.out.println("You rolled " + dicepair.getLastRoll() + "!");
        }

        System.out.println("Great job! It took you " + totalRolls + " rolls.");

    }
}
